# BoB schema coordinated-namespace

The purpose of the _Coordinated Ticket Name Space_ is to define a common set of variables and data structures to be used within the payloadData of the MTB (as specified in the [MTS1](https://bitbucket.org/samtrafiken/bob-mts-1-mobile-ticket-format/src/master/mts1.pdf) specification). It is REQUIRED to only use the properties and definitions as specified in [bob-schema-coordinated-name-space.json](https://bitbucket.org/samtrafiken/bob-schema-coordinated-namespace/src/master/coordinated-name-space.json) unless a private (proprietary) and unregistered extension (prefixed with "p_") is used.

This is a part of the [BoB - National Ticket and Payment Standard](https://bob.samtrafiken.se), hosted by Samtrafiken i Sverige AB